import React from 'react';
import styles from  './styles.module.scss';

function Main() {
  return (
      <div className={styles.app}>
        <h1>Main page</h1>
      </div>
  );
}

export default Main;
