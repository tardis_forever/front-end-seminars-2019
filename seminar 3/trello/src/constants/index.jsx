export const C_USER = {
    CHANGE_DATA: "CHANGE_DATA",
    CHANGE_AVATAR: "CHANGE_AVATAR",
    ADD_USER: "ADD_USER",
    REMOVE_USER: "REMOVE_USER"
};


export const USER_STATUS = {
    AUTH: "AUTH",
    NOT_AUTH: "NOT_AUTH",
};

export const C_BOARD = {
  ADD_TASK: "ADD_TASK",
  REMOVE_TASK: "REMOVE_TASK",
  CHANGE_TASK: "CHANGE_TASK",
};

export const MAIN_URL = {
    LOGIN: "/login",
    REGIST: "/regist/",
    HOME: "/home/:id",
    BOARD: "/board/"

};