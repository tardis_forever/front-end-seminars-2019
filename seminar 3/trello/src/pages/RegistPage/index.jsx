import React from 'react';
import Content from "../../components/Content";
import Header from "../../components/Header";
import RegistForm from "../../containers/RegistForm";


function RegistPage() {

    return <Content>
        <Header />
        <RegistForm/>
    </Content>;
}


export default RegistPage;