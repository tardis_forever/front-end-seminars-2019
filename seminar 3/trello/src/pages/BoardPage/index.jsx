import React from 'react';
import Header from "../../components/Header";
import {withRouter} from 'react-router-dom';
import UserInfo from "../../containers/UserInfo";

function BoardPage({match}) {
    console.log(match);

    return <div>
        <Header>
            <UserInfo />
        </Header>
    </div>;
}


export default withRouter(BoardPage);