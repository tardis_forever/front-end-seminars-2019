import React from 'react';
import styles from "../../pages/authPage/styles.module.scss";

import cn from 'classnames';

function Content({children, className}) {

    return <div className={cn(styles.content, className)}>
        {children}
    </div>;
}


export default Content;