import React from 'react';
import {NavLink} from "react-router-dom";
import {MAIN_URL} from "../../constants";
import styles from './styles.module.scss';

import logo_img from '../../assets/media/to-do-list.png';

function Header({...props}) {

    return <header className={styles.header}>
        <NavLink to={MAIN_URL.LOGIN}>
        <span className={styles.logo_content}>
            <img src={logo_img} alt="logo" className={styles.logo_img}/>
            ToDoList
        </span>
        </NavLink>
        {props.children}
    </header>
}

export default Header;