import {C_BOARD} from "../constants";

export const board = (state = [], action) => {
    switch (action.type) {
        case C_BOARD.ADD_TASK: {
            return state;
        }
        default:
            return state;
    }
};