import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {withRouter} from 'react-router-dom';
import {removeUser} from "../../actions/user";
import {MAIN_URL} from "../../constants";
import styles from './styles.module.scss';
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
function UserInfo({history, match}) {
    const {user} = useSelector(store => ({
        user: store.user,
    }));
    const dispatch = useDispatch();

    const [open, setOpen] = useState(false);

    const exit = () => {
        dispatch(removeUser());
        history.push(MAIN_URL.LOGIN);
    };

    return <span>
                <div onClick={() => setOpen(!open)} className={styles.user_info}>
                    <span style={{cursor: "pointer"}}>
                    {(user.first_name.length > 0 || user.last_name.length > 0)
                        ? `${user.first_name} ${user.last_name}` : user.login}
                    </span>

                    {open &&
                    <ClickAwayListener onClickAway={() => setOpen(false)}>
                        <div className={styles.drop_down}>
                        <button onClick={() => exit()}>Выход</button>
                    </div>
                    </ClickAwayListener>}

                </div>

            </span>;
}


export default withRouter(UserInfo);